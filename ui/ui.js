define('two/mintHelper/ui', [
    'two/mintHelper',
    'two/FrontButton',
    'two/locale',
    'two/utils',
    'two/eventQueue'
], function (
    mintHelper,
    FrontButton,
    Locale,
    utils,
    eventQueue
) {
    var opener

    function MinterInterface () {
        Locale.create('minter', __minter_locale, 'pl')
        
        opener = new FrontButton(Locale('minter', 'title'), {
            classHover: false,
            classBlur: false,
            tooltip: Locale('minter', 'description')
        })

        opener.click(function () {
            if (mintHelper.isRunning()) {
                mintHelper.stop()
                utils.emitNotif('success', Locale('minter', 'deactivated'))
            } else {
                mintHelper.start()
                utils.emitNotif('success', Locale('minter', 'activated'))
            }
        })

        eventQueue.bind('Minter/started', function () {
            opener.$elem.removeClass('btn-green').addClass('btn-red')
        })

        eventQueue.bind('Minter/stopped', function () {
            opener.$elem.removeClass('btn-red').addClass('btn-green')
        })

        if (mintHelper.isRunning()) {
            eventQueue.trigger('Minter/started')
        }

        return opener
    }

    mintHelper.interface = function () {
        mintHelper.interface = MinterInterface()
    }
})
