require([
    'two/ready',
    'two/mintHelper',
    'Lockr',
    'two/eventQueue',
    'two/mintHelper/ui',
], function (
    ready,
    mintHelper,
    Lockr,
    eventQueue
) {
    if (mintHelper.isInitialized()) {
        return false
    }

    ready(function () {
        mintHelper.init()
        mintHelper.interface()
        
        ready(function () {
            eventQueue.bind('Minter/started', function () {
                Lockr.set('minter-active', true)
            })

            eventQueue.bind('Minter/stopped', function () {
                Lockr.set('minter-active', false)
            })
        }, ['initial_village'])
    })
})
