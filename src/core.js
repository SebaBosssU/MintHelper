define('two/mintHelper', [
    'two/eventQueue',
    'two/utils',
    'Lockr'
], function(
    eventQueue,
    utils,
    Lockr
) {
    var initialized = false
    var running = false
    var recall = true
    var interval = 3000
    var modelDataService = injector.get('modelDataService')
    var socketService = injector.get('socketService')
    var routeProvider = injector.get('routeProvider')
	
    function mintCoins() {
        var player = modelDataService.getSelectedCharacter()
        var villages = player.getVillageList()
        villages.forEach(function(village) {
            var amountWood = 0
            var amountClay = 0
            var amountIron = 0
            var data = village.data
            var buildings = data.buildings
            var academy = buildings.academy
            var level = academy.level
            var resources = village.getResources()
            var computed = resources.getComputed()
            var maxStorage = resources.getMaxStorage()
            var wood = computed.wood
            var clay = computed.clay
            var iron = computed.iron
            var villageWood = wood.currentStock
            var villageClay = clay.currentStock
            var villageIron = iron.currentStock
            var woodCost = 28000
            var clayCost = 30000
            var ironCost = 25000
            setTimeout(function() {
                if (level > 0) {
                    if (villageWood >= woodCost && villageClay >= clayCost && villageIron >= ironCost) {
                        amountWood = Math.floor(villageWood / woodCost)
                        amountClay = Math.floor(villageClay / clayCost)
                        amountIron = Math.floor(villageIron / ironCost)
                        if (amountWood <= amountIron && amountWood <= amountClay) {
                            socketService.emit(routeProvider.MINT_COINS, {
                                village_id: village.getId(),
                                amount: amountWood
                            })
                            utils.emitNotif('success', 'W wiosce ' + village.getName() + ' wybito ' + amountWood + ' monet.')
                        } else if (amountClay <= amountIron && amountClay <= amountWood) {
                            socketService.emit(routeProvider.MINT_COINS, {
                                village_id: village.getId(),
                                amount: amountClay
                            })
                            utils.emitNotif('success', 'W wiosce ' + village.getName() + ' wybito ' + amountClay + ' monet.')
                        } else {
                            socketService.emit(routeProvider.MINT_COINS, {
                                village_id: village.getId(),
                                amount: amountIron
                            })
                            utils.emitNotif('success', 'W wiosce ' + village.getName() + ' wybito ' + amountIron + ' monet.')
                        }
                    } else {
                    }
                } else {
                }
            }, interval)
        })
        setTimeout(mintCoins, 30000)
    }
    var mintHelper = {}
    mintHelper.init = function() {
        initialized = true
    }
    mintHelper.start = function() {
        eventQueue.trigger('Minter/started')
        running = true
        mintCoins()
    }
    mintHelper.stop = function() {
        eventQueue.trigger('Minter/stopped')
        running = false
    }
    mintHelper.isRunning = function() {
        return running
    }
    mintHelper.isInitialized = function() {
        return initialized
    }
    return mintHelper
})